# CONTRIBUTORS
This file contains the list of people who have contributed to the Qulice and jSquint projects
throughout the years.

## jSquint
* Borislav Borisov (chmodas@gmail.com)

## Qulice
* Dmitry Bashkin (dmitry.bashkin@qulice.com)
* Yegor Bugayenko (yegor@tpc2.com)
* Hamdi Douss (douss.hamdi@gmail.com)
* Krzysztof Krason (Krzysztof.Krason@gmail.com)
* Paul Polishchuk (ppol@ua.fm)
* Viktor Kuchyn (kuchin.victor@gmail.com)
* Jimmy Spivey (JimDeanSpivey@gmail.com)
* Denys Skalenko (d.skalenko@gmail.com)
* Dzmitry Petrushenka (dpetruha@gmail.com)
* Pavlo Shamrai (pshamrai@gmail.com)
* Prahlad Yeri (prahladyeri@yahoo.com)

## Contributions
To retrieve a full list of the author's contribution please run the following command:

```bash
git log --pretty="%H" --author="<INSERT AUTHOR HERE>" | while read commit_hash; do git show --oneline --name-only $commit_hash | tail -n+2; done | sort | uniq
```

