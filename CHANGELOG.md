# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project
adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

### Changed

### Removed

## 0.3.0 - 2018-01-03
The last, hopefully, "clean up" release, which removes duplicate and controversial checks,
unnecessary modules, leftovers from the jcabi-parent.

## Added
A [CONTRIBUTORS](CONTRIBUTORS.md) list; any @author has been added to the list. 

### Changed
- Suppress the ClassDataAbstractionCouplingCheck for the test suite classes
- Clean up the JavaDoc a bit, remove any unused @checkstyle and @SuppressWarnings noise. 

### Removed
- The **codenarc** validator. This is a Java-only tool.
- The EmptyLinesCheck.
- Duplicated checkstyle and PMD checks (displayed as kept -> removed pairs):
  * PMD.ImportFromSamePackage -> checkstyle:RedundantImport
  * PMD.UselessParentheses -> checkstyle:UnnecessaryParentheses & PMD.UnnecessaryParentheses

## 0.2.0 - 2017-11-11
Removed duplicate checks (performed by both checkstyle and PMD), few controversial checks and the 
ones handled by the IDEs already.

### Added
Absolutely nothing.

### Changed
- Allow numbers in variable names.
- Allow variables that are only 1 character long.
- EmptyCatchBlock ignores statements with their variable name set to "ignored" or "expected".

### Removed
- Controversial checks:
  * PMD.AvoidFieldNameMatchingMethodName - this is just silly
  * VisibilityModifier
  * StringLiteralsConcatenation - you should know better when to use concatenation or not
  * HiddenField - the code style promoted by these settings revolves around immutability 
  and catching a problem caused by a shadowing field should be trivial
  * ImportCohesionCheck - IDEs handle this well
  * JUnitAssertionsShouldIncludeMessage

- Duplicated checkstyle and PMD checks (displayed as kept -> removed pairs):
  * IllegalCatch -> PMD.AvoidCatchingGenericException
  * PMD.UnusedImports -> UnusedImports
  * RedundantModifier -> PMD.UnnecessaryFinalModifier
  * NeedBraces -> PMD.IfStmtsMustUseBraces
  * PMD: AbstractNaming -> AbstractClassName
  * EmptyCatchBlock -> PMD.EmptyCatchBlock
  
The checks that were kept are the ones with more options.
  
## 0.1.0 - 2017-09-26
Forked from [Qulice](http:/www.qulice.com), version
[0.17.1](https://github.com/teamed/qulice/tree/0.17.1).

### Added
- checkstyle audit event suppression with annotations (e.g. @SuppressWarnings("checkstyle:<ID>")).
  The @checkstyle Javadoc tag is still supported, mostly for backward-compatibility.
- Suppression for certain checkstyle checks for tests files - the ones the in ```src/test```
  and ```src/it``` directories:
  * EmptyBlock
  * ImportControl
  * MagicNumber
  * AvoidStaticImport
  * MethodCount 
  * MultipleStringLiterals
  * Javado
  * JavadocVariabl
  * JavadocLocationCheck

### Changed
- The style guide to allow camelCase and longer variable names, and shorter in certain cases, such
  as the widely-used "e" for exceptions.
- The line maximum length from 80 to 100.
- The Javadoc tags requirement is limited to @param, @return, @throws, @deprecated. The rest are
  subject to change far too often for most applications to enforce them by default.

### Removed
- All validators except for checkstyle, PMD, FindBugs and CodeNarc.
- A number of checkstyle and PMD checks, rigid and idealistic, often leading to less-readable code.
- Checks duplication. If checkstyle already checked it PMD won't.
