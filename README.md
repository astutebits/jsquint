# jSquint
**jSquint** is a static analysis and programming style control instrument for Java projects.

jSquint is a fork of [Qulice] that puts less emphasis on rigid rules and more on promoting good
practices, while enforcing an unified code style that favours immutability. While the idea remains 
the same, jSquint employees far less checks than the version of Qulice (0.17.1) it's based off. 
Presently the checks it performs, with a set of predefined settings, are checkstyle, PMD, FindBugs, 
and CodeNarc - if any ".groovy" files are found.

## Changelog
All notable changes to this project are documented in [the CHANGELOG](CHANGELOG.md) file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## Usage
Simply add the ```jsquint-maven-plugin``` to your Maven project and configure the "jsquint:check"
goal to run as part of your build. The default cycle of the goal is set to "verify".

```xml
    <build>
        <plugins>
            <plugin>
                <groupId>com.astutebits</groupId>
                <artifactId>jsquint-maven-plugin</artifactId>
                <version>0.3.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
````

[Qulice]: http://www.qulice.com
[Keep a Changelog]: http://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: http://semver.org/spec/v2.0.0.html.
