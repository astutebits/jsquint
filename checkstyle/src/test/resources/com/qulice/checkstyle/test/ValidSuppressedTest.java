/**
 * Hello.
 */
package foo;

/**
 * Checkstyle suppression should work for this.
 */
public final class ValidSuppressedTest {
    /**
     * Yeah!
     *
     * @return A number.
     */
    public int errorless() {
        return 5;
    }
}
