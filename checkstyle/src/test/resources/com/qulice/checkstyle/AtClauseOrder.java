/**
 * Hello.
 */
package foo;

/**
 * Correct Javadoc for class {@link AtClauseOrder}.
 */
public final class AtClauseOrder {
    /**
     * Ah, Qulice...
     */
    private final String whoCares;

    /**
     * Constructor.
     *
     * @param noOne No one does.
     */
    public AtClauseOrder(final String noOne) {
        this.whoCares = noOne;
    }

    /**
     * Just a method with invalid Javadoc.
     *
     * @return Some value
     * @param input The input!
     */
    public static int secondMethod(final int input) {
        return input;
    }
}
